<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Api\CreateApiActionInterface;

interface SessionCreateApiActionInterface extends CreateApiActionInterface
{}
