<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Api\GetOneByIdApiActionInterface;

interface SessionGetOneByIdApiActionInterface extends GetOneByIdApiActionInterface
{}
