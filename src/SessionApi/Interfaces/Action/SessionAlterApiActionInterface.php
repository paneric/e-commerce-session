<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Api\AlterApiActionInterface;

interface SessionAlterApiActionInterface extends AlterApiActionInterface
{}
