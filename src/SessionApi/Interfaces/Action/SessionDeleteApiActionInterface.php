<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Api\DeleteApiActionInterface;

interface SessionDeleteApiActionInterface extends DeleteApiActionInterface
{}
