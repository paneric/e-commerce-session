<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {

    $app->get('/api-sess/{page}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->getAllPaginated(
            $request,
            $response,
            $this->get('session_get_all_paginated_action'),
            $args['page']
        );
    })->setName('get.api-sess.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-sess', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->getAll(
            $request,
            $response,
            $this->get('session_get_all_action')
        );
    })->setName('get.api-sess')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-ses/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->getOneById(
            $response,
            $this->get('session_get_one_by_id_action'),
            $args['id']
        );
    })->setName('get.api-ses')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-ses', function (Request $request, Response $response) {
        return $this->get('session_controller')->create(
            $request,
            $response,
            $this->get('session_create_action')
        );
    })->setName('post.api-ses')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-ses/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->alter(
            $request,
            $response,
            $this->get('session_alter_action'),
            $args['id']
        );
    })->setName('put.api-ses')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->delete('/api-ses/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->delete(
            $request,
            $response,
            $this->get('session_delete_action'),
            $args['id']
        );
    })->setName('delete.api-ses')
        ->addMiddleware($container->get(JwtAuthentication::class));
}
