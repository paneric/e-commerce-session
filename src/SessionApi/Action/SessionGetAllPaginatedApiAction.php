<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionGetAllPaginatedApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetAllPaginatedApiAction;

class SessionGetAllPaginatedApiAction
    extends GetAllPaginatedApiAction
    implements SessionGetAllPaginatedApiActionInterface
{}
