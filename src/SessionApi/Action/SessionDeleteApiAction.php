<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionDeleteApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\DeleteApiAction;

class SessionDeleteApiAction
    extends DeleteApiAction
    implements SessionDeleteApiActionInterface
{}
