<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionGetAllApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetAllApiAction;

class SessionGetAllApiAction
    extends GetAllApiAction
    implements SessionGetAllApiActionInterface
{}
