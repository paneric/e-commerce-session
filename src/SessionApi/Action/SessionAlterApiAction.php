<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionAlterApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\AlterApiAction;

class SessionAlterApiAction
    extends AlterApiAction
    implements SessionAlterApiActionInterface
{}
