<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionCreateApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\CreateApiAction;

class SessionCreateApiAction
    extends CreateApiAction
    implements SessionCreateApiActionInterface
{}
