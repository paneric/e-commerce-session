<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi\Action;

use ECommerce\Session\SessionApi\Interfaces\Action\SessionGetOneByIdApiActionInterface;
use Paneric\BaseModule\Module\Action\Api\GetOneByIdApiAction;

class SessionGetOneByIdApiAction
    extends GetOneByIdApiAction
    implements SessionGetOneByIdApiActionInterface
{}
