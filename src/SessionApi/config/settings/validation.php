<?php

declare(strict_types=1);

use ECommerce\Session\Gateway\SessionDTO;

return [

    'validation' => [

        'post.api-ses' => [
            'methods' => ['POST'],
            SessionDTO::class => [
                'rules' => [
                    'session_id' => [
                    ], 
                    'ip_address' => [
                    ], 
                    'user_agent' => [
                    ], 
                    'data' => [
                    ], 

                ],
            ],
        ],

        'put.api-ses' => [
            'methods' => ['PUT'],
            SessionDTO::class => [
                'rules' => [
                    'session_id' => [
                    ], 
                    'ip_address' => [
                    ], 
                    'user_agent' => [
                    ], 
                    'data' => [
                    ], 

                ],
            ],
        ],
    ]
];
