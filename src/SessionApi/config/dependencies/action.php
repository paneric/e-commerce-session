<?php

declare(strict_types=1);

use ECommerce\Session\Gateway\SessionDAO;
use ECommerce\Session\Gateway\SessionDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Validation\ValidationService;

use ECommerce\Session\SessionApi\Action\SessionAlterApiAction;
use ECommerce\Session\SessionApi\Action\SessionCreateApiAction;
use ECommerce\Session\SessionApi\Action\SessionDeleteApiAction;
use ECommerce\Session\SessionApi\Action\SessionGetAllApiAction;
use ECommerce\Session\SessionApi\Action\SessionGetAllPaginatedApiAction;
use ECommerce\Session\SessionApi\Action\SessionGetOneByIdApiAction;

return [
    'session_create_action' => static function (Container $container): SessionCreateApiAction {
        return new SessionCreateApiAction(
            $container->get('session_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => SessionDAO::class,
                'dto_class' => SessionDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['ses_session_id' => $attributes['session_id']];
                },
            ]
        );
    },
    'session_alter_action' => static function (Container $container): SessionAlterApiAction {
        return new SessionAlterApiAction(
            $container->get('session_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => SessionDAO::class,
                'dto_class' => SessionDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['ses_id' => (int) $id, 'ses_session_id' => $attributes['session_id']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
    'session_delete_action' => static function (Container $container): SessionDeleteApiAction {
        return new SessionDeleteApiAction(
            $container->get('session_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
    'session_get_all_action' => static function (Container $container): SessionGetAllApiAction {
        return new SessionGetAllApiAction(
            $container->get('session_repository'),
            [
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'session_get_all_paginated_action' => static function (Container $container): SessionGetAllPaginatedApiAction {
        return new SessionGetAllPaginatedApiAction(
            $container->get('session_repository'),
            [
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'session_get_one_by_id_action' => static function (Container $container): SessionGetOneByIdApiAction {
        return new SessionGetOneByIdApiAction(
            $container->get('session_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
];
