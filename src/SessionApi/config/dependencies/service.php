<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Validation\ValidationService;
use Paneric\Validation\Validator;
use Paneric\Validation\ValidatorBuilder;

return [
    Validator::class => static function (Container $container): Validator {
        $validatorBuilder = new ValidatorBuilder();

        return $validatorBuilder->build(
            (string) $container->get('local')
        );
    },

    ValidationService::class => static function (Container $container): ValidationService {
        $validation = (array) $container->get('validation');

        if ($container->has('seo-validation')) {
            $validation = array_merge($validation, (array) $container->get('seo-validation'));
        }

        return new ValidationService(
            $container->get(Validator::class),
            $validation
        );
    },
];
