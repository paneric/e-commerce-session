<?php

declare(strict_types=1);

use Paneric\DBAL\DataPreparator;
use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\SequencePreparator;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\Guard\Guard;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Session\DBAL\DBALAdapter;
use Paneric\Session\DBAL\SessionRepository;
use Paneric\Session\SessionWrapper;
use Slim\App;
use Slim\Factory\AppFactory;

return [
    App::class => static function (Container $container) {
        AppFactory::setContainer($container);

        return AppFactory::create();
    },

    'route_parser_interface' => static function (Container $container) {
        $app = $container->get(App::class);

        return $app->getRouteCollector()->getRouteParser();
    },

    Manager::class => static function (Container $container): Manager
    {
        $pdoBuilder = new PDOBuilder();

        return new Manager(
            $pdoBuilder->build($container->get('dbal')),
            new QueryBuilder(new SequencePreparator()),
            new DataPreparator()
        );
    },

    GuardInterface::class => static function (Container $container): Guard
    {
        $randomFactory = new RandomLib\Factory;

        return new Guard(
            $randomFactory->getMediumStrengthGenerator(),
            $container->get('guard')
        );
    },

    SessionInterface::class => static function (Container $container): ?SessionWrapper
    {
        $pdoAdapter = new DBALAdapter(
            $container->get(Manager::class),
            new SessionRepository($container->get(Manager::class)),
            $container->get(GuardInterface::class)
        );

        return new SessionWrapper(
            $container->get('session-wrapper'),
            $pdoAdapter
        );
    },
];
