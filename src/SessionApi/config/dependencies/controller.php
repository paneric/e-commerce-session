<?php

declare(strict_types=1);

use ECommerce\Session\SessionApi\SessionApiController;

return [
    'session_controller' => static function (): SessionApiController {
        return new SessionApiController();
    },
];
