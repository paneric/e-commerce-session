<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApi;

use Paneric\BaseModule\Module\Controller\ModuleApiController;

class SessionApiController extends ModuleApiController {}
