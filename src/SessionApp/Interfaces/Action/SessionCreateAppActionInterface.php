<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\App\CreateActionInterface;

interface SessionCreateAppActionInterface extends CreateActionInterface
{}
