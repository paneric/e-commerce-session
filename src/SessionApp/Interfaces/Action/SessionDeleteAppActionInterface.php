<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\App\DeleteActionInterface;

interface SessionDeleteAppActionInterface extends DeleteActionInterface
{}
