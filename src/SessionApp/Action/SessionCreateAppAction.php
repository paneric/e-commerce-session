<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionCreateAppActionInterface;
use Paneric\BaseModule\Module\Action\App\CreateAppAction;

class SessionCreateAppAction
    extends CreateAppAction
    implements SessionCreateAppActionInterface
{}
