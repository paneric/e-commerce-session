<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionGetAllPaginatedAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetAllPaginatedAppAction;

class SessionGetAllPaginatedAppAction
    extends GetAllPaginatedAppAction
    implements SessionGetAllPaginatedAppActionInterface
{}
