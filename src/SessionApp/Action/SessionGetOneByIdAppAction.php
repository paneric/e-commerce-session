<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionGetOneByIdAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetOneByIdAppAction;

class SessionGetOneByIdAppAction
    extends GetOneByIdAppAction
    implements SessionGetOneByIdAppActionInterface
{}
