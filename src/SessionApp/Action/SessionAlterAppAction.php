<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionAlterAppActionInterface;
use Paneric\BaseModule\Module\Action\App\AlterAppAction;

class SessionAlterAppAction
    extends AlterAppAction
    implements SessionAlterAppActionInterface
{}
