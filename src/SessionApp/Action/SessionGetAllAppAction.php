<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionGetAllAppActionInterface;
use Paneric\BaseModule\Module\Action\App\GetAllAppAction;

class SessionGetAllAppAction
    extends GetAllAppAction
    implements SessionGetAllAppActionInterface
{}
