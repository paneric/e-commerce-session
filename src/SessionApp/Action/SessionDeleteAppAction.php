<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp\Action;

use ECommerce\Session\SessionApp\Interfaces\Action\SessionDeleteAppActionInterface;
use Paneric\BaseModule\Module\Action\App\DeleteAppAction;

class SessionDeleteAppAction
    extends DeleteAppAction
    implements SessionDeleteAppActionInterface
{}
