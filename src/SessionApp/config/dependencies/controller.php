<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\Session\SessionApp\SessionAppController;
use Twig\Environment as Twig;

return [
    'session_controller' => static function (Container $container): SessionAppController {
        return new SessionAppController(
            $container->get(Twig::class),
            'ses'
        );
    },
];
