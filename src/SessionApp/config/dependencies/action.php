<?php

declare(strict_types=1);

use ECommerce\Session\Gateway\SessionDAO;
use ECommerce\Session\Gateway\SessionDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Validation\ValidationService;

use ECommerce\Session\SessionApp\Action\SessionAlterAppAction;
use ECommerce\Session\SessionApp\Action\SessionCreateAppAction;
use ECommerce\Session\SessionApp\Action\SessionDeleteAppAction;
use ECommerce\Session\SessionApp\Action\SessionGetAllAppAction;
use ECommerce\Session\SessionApp\Action\SessionGetAllPaginatedAppAction;
use ECommerce\Session\SessionApp\Action\SessionGetOneByIdAppAction;

return [
    'session_create_action' => static function (Container $container): SessionCreateAppAction {
        return new SessionCreateAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'session',
                'prefix' => 'ses',
                'dao_class' => SessionDAO::class,
                'dto_class' => SessionDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['ses_session_id' => $attributes['session_id']];
                },
            ]
        );
    },
    'session_alter_action' => static function (Container $container): SessionAlterAppAction {
        return new SessionAlterAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'session',
                'prefix' => 'ses',
                'dao_class' => SessionDAO::class,
                'dto_class' => SessionDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['ses_id' => (int) $id, 'ses_session_id' => $attributes['session_id']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
    'session_delete_action' => static function (Container $container): SessionDeleteAppAction {
        return new SessionDeleteAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'session',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
    'session_get_all_action' => static function (Container $container): SessionGetAllAppAction {
        return new SessionGetAllAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'session',
                'prefix' => 'ses',
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'session_get_all_paginated_action' => static function (Container $container): SessionGetAllPaginatedAppAction {
        return new SessionGetAllPaginatedAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'session',
                'prefix' => 'ses',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    },
    'session_get_one_by_id_action' => static function (Container $container): SessionGetOneByIdAppAction {
        return new SessionGetOneByIdAppAction(
            $container->get('session_repository'),
            $container->get(SessionInterface::class),
            [
                'prefix' => 'ses',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['ses_id' => (int) $id];
                },
            ]
        );
    },
];
