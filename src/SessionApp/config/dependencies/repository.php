<?php

declare(strict_types=1);

use ECommerce\Session\Gateway\SessionDAO;
use ECommerce\Session\Repository\SessionRepository;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\DBAL\Manager;

return [
    'session_repository' => static function (Container $container): SessionRepository
    {
        return new SessionRepository(
            $container->get(Manager::class),
            [
                'table' => 'sessions',
                'dao_class' => SessionDAO::class,
                'create_unique_where' => sprintf(
                    ' %s',
                    'WHERE ses_session_id=:ses_session_id'
                ),
                'update_unique_where' => sprintf(
                    ' %s %s',
                    'WHERE ses_session_id=:ses_session_id',
                    'AND ses_id NOT IN (:ses_id)'
                ),
            ]
        );
    },
];
