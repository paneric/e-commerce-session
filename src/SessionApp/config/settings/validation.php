<?php

declare(strict_types=1);

use ECommerce\Session\Gateway\SessionDTO;

return [

    'validation' => [

        'ses.add' => [
            'methods' => ['POST'],
            SessionDTO::class => [
                'rules' => [
                    'session_id' => [
                    ], 
                    'ip_address' => [
                    ], 
                    'user_agent' => [
                    ], 
                    'data' => [
                    ], 

                ],
            ],
        ],

        'ses.edit' => [
            'methods' => ['POST'],
            SessionDTO::class => [
                'rules' => [
                    'session_id' => [
                    ], 
                    'ip_address' => [
                    ], 
                    'user_agent' => [
                    ], 
                    'data' => [
                    ], 

                ],
            ],
        ],

        'ses.remove' => [
            'methods' => ['POST'],
            SessionDTO::class => [
                'rules' => [],
            ],
        ],
    ]
];
