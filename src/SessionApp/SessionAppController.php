<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApp;

use Paneric\BaseModule\Module\Controller\ModuleAppController;

class SessionAppController extends ModuleAppController {}
