<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/ses/show-all', function (Request $request, Response $response) {
        return $this->get('session_controller')->showAll(
            $response,
            $this->get('session_get_all_action')
        );
    })->setName('ses.show_all');

    $app->get('/ses/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->showOneById(
            $response,
            $this->get('session_get_one_by_id_action'),
            $args['id']
        );
    })->setName('ses.show_one_by_id');

    $app->map(['GET', 'POST'], '/ses/add', function (Request $request, Response $response) {
        return $this->get('session_controller')->add(
            $request,
            $response,
            $this->get('session_create_action')
        );
    })->setName('ses.add')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/ses/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->edit(
            $request,
            $response,
            $this->get('session_alter_action'),
            $args['id']
        );
    })->setName('ses.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/ses/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->remove(
            $request,
            $response,
            $this->get('session_delete_action'),
            $args['id']
        );
    })->setName('ses.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class));
}
