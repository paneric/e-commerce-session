<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/ses/show-all-paginated[/{page}]', function (Request $request, Response $response) {
        return $this->get('session_controller')->showAllPaginated(
            $request,
            $response,
            $this->get('session_get_all_paginated_action')
        );
    })->setName('ses.show_all_paginated')
        ->addMiddleware($container->get(PaginationMiddleware::class));
}
