<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Interfaces\Action;

use Paneric\BaseModule\Interfaces\Action\Apc\GetAllPaginatedActionInterface;

interface SessionGetAllPaginatedApcActionInterface extends GetAllPaginatedActionInterface
{}
