<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc;

use Paneric\BaseModule\Module\Controller\ModuleApcController;

class SessionApcController extends ModuleApcController {}
