<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/apc-ses/show-all', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->showAll(
            $request,
            $response,
            $this->get('session_show_all_action')
        );
    })->setName('apc-ses.show_all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->get('/apc-ses/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->showOneById(
            $request,
            $response,
            $this->get('session_show_one_by_id_action'),
            $args['id']
        );
    })->setName('apc-ses.show_one_by_id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ses/add', function (Request $request, Response $response) {
        return $this->get('session_controller')->add(
            $request,
            $response,
            $this->get('session_add_action')
        );
    })->setName('apc-ses.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ses/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->edit(
            $request,
            $response,
            $this->get('session_edit_action'),
            $this->get('session_show_one_by_id_action')->getOneById($request, $args['id']),
            $args['id']
        );
    })->setName('apc-ses.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-ses/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('session_controller')->remove(
            $request,
            $response,
            $this->get('session_remove_action'),
            $args['id']
        );
    })->setName('apc-ses.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
