<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionGetAllPaginatedApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetAllPaginatedApcAction;

class SessionGetAllPaginatedApcAction
    extends GetAllPaginatedApcAction
    implements SessionGetAllPaginatedApcActionInterface
{}
