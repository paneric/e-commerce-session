<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionGetOneByIdApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetOneByIdApcAction;

class SessionGetOneByIdApcAction
    extends GetOneByIdApcAction
    implements SessionGetOneByIdApcActionInterface
{}
