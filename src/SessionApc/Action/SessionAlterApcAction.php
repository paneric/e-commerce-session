<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionAlterApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\AlterApcAction;

class SessionAlterApcAction
    extends AlterApcAction
    implements SessionAlterApcActionInterface
{}
