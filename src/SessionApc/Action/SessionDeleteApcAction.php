<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionDeleteApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\DeleteApcAction;

class SessionDeleteApcAction
    extends DeleteApcAction
    implements SessionDeleteApcActionInterface
{}
