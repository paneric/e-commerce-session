<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionGetAllApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\GetAllApcAction;

class SessionGetAllApcAction
    extends GetAllApcAction
    implements SessionGetAllApcActionInterface
{}
