<?php

declare(strict_types=1);

namespace ECommerce\Session\SessionApc\Action;

use ECommerce\Session\SessionApc\Interfaces\Action\SessionCreateApcActionInterface;
use Paneric\BaseModule\Module\Action\Apc\CreateApcAction;

class SessionCreateApcAction
    extends CreateApcAction
    implements SessionCreateApcActionInterface
{}
