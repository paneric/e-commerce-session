<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\Session\SessionApc\SessionApcController;
use Twig\Environment as Twig;

return [
    'session_controller' => static function (Container $container): SessionApcController {
        return new SessionApcController(
            $container->get(Twig::class),
            'apc-ses'
        );
    },
];
