<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use ECommerce\Session\SessionApc\Action\SessionAlterApcAction;
use ECommerce\Session\SessionApc\Action\SessionCreateApcAction;
use ECommerce\Session\SessionApc\Action\SessionDeleteApcAction;
use ECommerce\Session\SessionApc\Action\SessionGetAllApcAction;
use ECommerce\Session\SessionApc\Action\SessionGetAllPaginatedApcAction;
use ECommerce\Session\SessionApc\Action\SessionGetOneByIdApcAction;

return [
    'session_add_action' => static function (Container $container): SessionCreateApcAction {
        return new SessionCreateApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'session',
                    'prefix' => 'ses'
                ]
            )
        );
    },
    'session_edit_action' => static function (Container $container): SessionAlterApcAction {
        return new SessionAlterApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'session',
                    'prefix' => 'ses'
                ]
            )
        );
    },
    'session_remove_action' => static function (Container $container): SessionDeleteApcAction {
        return new SessionDeleteApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                ['module_name_sc' => 'session']
            )
        );
    },
    'session_show_all_action' => static function (Container $container): SessionGetAllApcAction {
        return new SessionGetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'session',
                    'prefix' => 'ses'
                ]
            )

        );
    },
    'session_show_all_paginated_action' => static function (Container $container): SessionGetAllPaginatedApcAction {
        return new SessionGetAllPaginatedApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'session',
                    'prefix' => 'ses'
                ]
            )

        );
    },
    'session_show_one_by_id_action' => static function (Container $container): SessionGetOneByIdApcAction {
        return new SessionGetOneByIdApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'session',
                    'prefix' => 'ses'
                ]
            )
        );
    },
];
