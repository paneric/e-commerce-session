<?php

declare(strict_types=1);

namespace ECommerce\Session\Gateway;

use Paneric\DataObject\DAO;

class SessionDAO extends DAO
{
    protected $id;
    protected $sessionId;//String 
    protected $ipAddress;//String 
    protected $userAgent;//String 
    protected $data;//String 

    public function __construct()
    {
        $this->prefix = 'ses_';

        $this->setMaps();
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
    public function getSessionId(): ?String
    {
        return $this->sessionId;
    } 
    public function getIpAddress(): ?String
    {
        return $this->ipAddress;
    } 
    public function getUserAgent(): ?String
    {
        return $this->userAgent;
    } 
    public function getData(): ?String
    {
        return $this->data;
    } 

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    public function setSessionId(String $sessionId): void
    {
        $this->sessionId = $sessionId;
    } 
    public function setIpAddress(String $ipAddress): void
    {
        $this->ipAddress = $ipAddress;
    } 
    public function setUserAgent(String $userAgent): void
    {
        $this->userAgent = $userAgent;
    } 
    public function setData(String $data): void
    {
        $this->data = $data;
    } 

}
