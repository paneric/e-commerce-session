<?php

declare(strict_types=1);

return [
    'default_route_key' => 'ses',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => false,
    'module_map' => [
        'ses' => ROOT_FOLDER . 'src/SessionApp',
        'api-ses' => ROOT_FOLDER . 'src/SessionApi',
        'api-sess' => ROOT_FOLDER . 'src/SessionApi',
        'apc-ses' => ROOT_FOLDER . 'src/SessionApc',
    ],
    'module_map_cross' => [
        'ses'       => ROOT_FOLDER . 'vendor/paneric/e-commerce-session/src/SessionApp',
        'api-ses'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-session/src/SessionApi',
        'api-sess'  => ROOT_FOLDER . 'vendor/paneric/e-commerce-session/src/SessionApi',
        'apc-ses'   => ROOT_FOLDER . 'vendor/paneric/e-commerce-session/src/SessionApc',
    ],
];
