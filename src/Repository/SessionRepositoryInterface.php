<?php

declare(strict_types=1);

namespace ECommerce\Session\Repository;

use Paneric\BaseModule\Interfaces\Repository\ModuleRepositoryInterface;

interface SessionRepositoryInterface extends ModuleRepositoryInterface
{}
