<?php

declare(strict_types=1);

namespace ECommerce\Session\Repository;

use Paneric\BaseModule\Module\Repository\ModuleRepository;

class SessionRepository extends ModuleRepository implements SessionRepositoryInterface
{}
