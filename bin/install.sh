#!/bin/bash
clear

echo "COMPOSER"

composer update

echo "MIGRATIONS"

bin/phinx migrate -e development

echo "FIXTURES"

bin/phinx seed:run -e development

echo "INSTALLATION FINISHED"
