<?php

declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class SessionMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $table = $this->table('sessions', ['id' => 'ses_id']);
        $table->addColumn('ses_session_id', 'string', ['length' => 255, 'collation' => 'utf8_unicode_ci'])
            ->addColumn('ses_ip_address', 'string', ['length' => 255, 'collation' => 'utf8_unicode_ci'])
            ->addColumn('ses_user_agent', 'string', ['length' => 255, 'collation' => 'utf8_unicode_ci'])
            ->addColumn('ses_data', 'text')
            ->addColumn('ses_created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP'])
            ->addColumn('ses_updated_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP', 'update' => 'CURRENT_TIMESTAMP'])
            ->addIndex(['ses_session_id'], ['unique' => true])
            ->addIndex(['ses_ip_address', 'ses_user_agent', 'ses_created_at', 'ses_updated_at'])
            ->create();
    }
}
