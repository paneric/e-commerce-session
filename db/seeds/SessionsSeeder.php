<?php

use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class SessionsSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for ($i = 0; $i < 100; $i++) {
            $data[] = [
                'ses_session_id'      => $faker->md5,
                'ses_ip_address'      => $faker->unique()->ipv4,
                'ses_user_agent' => $faker->userAgent,
                'ses_data'         => $faker->sha256,
                'ses_created_at'    => date('Y-m-d H:i:s'),
                'ses_updated_at'     => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('sessions')->insert($data)->saveData();
    }
}
