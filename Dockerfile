FROM php:7.4-apache

RUN apt-get update && apt-get upgrade -y
RUN docker-php-ext-install bcmath pdo_mysql

#.htaccess:
RUN a2enmod rewrite
RUN a2enmod actions

#Project files:
RUN mkdir /e-commerce/
RUN mkdir /e-commerce/e-commerce-session/
COPY ./ /e-commerce/e-commerce-session/

#RUN mkdir /var/www/e-commerce-session/
#COPY ./ /var/www/e-commerce-session/

#Allowoverride All:
COPY docker-conf/httpd.conf /usr/local/apache2/conf/httpd.conf
#Document root:
COPY docker-conf/000-default.conf /etc/apache2/sites-available/000-default.conf

#php.ini:
COPY docker-conf/php-development.ini /usr/local/etc/php/php.ini

RUN apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y